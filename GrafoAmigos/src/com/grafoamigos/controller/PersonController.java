package com.grafoamigos.controller;

import java.util.ArrayList;

import com.grafoamigos.dao.PersonDAO;
import com.grafoamigos.model.Person;;


public class PersonController {
	public ArrayList<Person> getAllPeople(){
		System.out.println("PersonController - get all people");
		return PersonDAO.getInstance().getAllPeople();
	}
	
	public ArrayList<Person> getFriends(int id){
		System.out.println("PersonController - get friends of the " + id);
		return PersonDAO.getInstance().getFriends(id);
	}
}
