package com.grafoamigos.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.grafoamigos.factory.ConnectionFactory;
import com.grafoamigos.model.Person;

public class PersonDAO extends ConnectionFactory {

	private static PersonDAO instance;
	
	public static PersonDAO getInstance(){
		if(instance == null)
			instance = new PersonDAO();
		return instance;
	}
	
	
	public ArrayList<Person> getAllPeople(){
		Connection conexao = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Person> people = null;
		conexao = createConnection();
		people = new ArrayList<Person>();
		
		try {
			ps = conexao.prepareStatement("select * from people");
			rs = ps.executeQuery();
			
			while(rs.next()){
				Person person = new Person();
				person.setId(rs.getInt("id"));
				person.setName(rs.getString("name"));				
				people.add(person);
			}
			
		} catch (Exception e) {
			System.out.println("Error to get all people: " + e);
			e.printStackTrace();
		} finally {
			closeConnection(conexao, ps, rs);
		}
		return people;
	}
	
	public ArrayList<Person> getFriends(int id){
		Connection conexao = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Person> people = null;
		conexao = createConnection();
		people = new ArrayList<Person>();
		
		try {
			ps = conexao.prepareStatement("select f.friend from people p left join friendship f on f.person_node = " + id + "and f.person_node = p.id where f.id is not null;");
			rs = ps.executeQuery();
			
			while(rs.next()){
				Person person = new Person();
				person.setName(rs.getString("friend"));				
				people.add(person);
			}
			
		} catch (Exception e) {
			System.out.println("Error to get all people: " + e);
			e.printStackTrace();
		} finally {
			closeConnection(conexao, ps, rs);
		}
		return people;
	}
	
	public ArrayList<Person> getSuggestions(int id){
		Connection conexao = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Person> people = null;
		conexao = createConnection();
		people = new ArrayList<Person>();
		
		try {
			ps = conexao.prepareStatement("select * from friendship;");
			rs = ps.executeQuery();
			
			while(rs.next()){
				Person person = new Person();
				person.setId(id);
				person.setName(rs.getString("friend"));				
				people.add(person);
			}
			
		} catch (Exception e) {
			System.out.println("Error to get all people: " + e);
			e.printStackTrace();
		} finally {
			closeConnection(conexao, ps, rs);
		}
		return people;
	}
	
}
