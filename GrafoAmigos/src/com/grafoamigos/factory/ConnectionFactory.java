package com.grafoamigos.factory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class ConnectionFactory {

	private static final String DRIVER = "org.postgresql.Driver";
	private static final String URL = "jdbc:postgresql://localhost:5432/friends";
	private static final String USER = "postgres";
	private static final String PASSWORD = "postgres";
	
	public Connection createConnection(){
		
		Connection conexao = null;
		
		try {
			Class.forName(DRIVER);
			conexao = DriverManager.getConnection(URL, USER, PASSWORD);
		} catch (Exception e) {
			System.out.println("Error to connect with database: " + URL);
			e.printStackTrace();
		}
		
		return conexao;
	}
	
	
	public void closeConnection(Connection conn, PreparedStatement ps, ResultSet rs){
		
		try {
			if(conn != null){
				conn.close();
			}
			if(ps != null){
				ps.close();
			}
			if(rs != null){
				rs.close();
			}
					
		} catch (Exception e) {
			System.out.println("Error to close connection: " + URL);
		}
	}
}