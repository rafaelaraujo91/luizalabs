package com.grafoamigos.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public final class Person {
	
	private Integer id;
	private String name;
	private List<Integer>listFriends;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Integer> getListFriends() {
		return listFriends;
	}
	public void setListFriends(List<Integer> listFriends) {
		this.listFriends = listFriends;
	}
	
}
