package com.grafoamigos.resource;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.grafoamigos.controller.PersonController;
import com.grafoamigos.model.Person;

@Path("/friends")
public class PersonResource {
	
	@GET
	@Path("/getAll")
	@Produces("application/xml")
	public ArrayList<Person> getAllPeople(){
		return new PersonController().getAllPeople();
	}
	
	@GET
	@Path("/getFriends/{id}")
	@Produces("application/xml")
	public ArrayList<Person> getFriends(@PathParam("id") String id){
		System.out.println("PersonResource - get friends of the " + id);
		return new PersonController().getFriends(Integer.parseInt(id));
	}
}
