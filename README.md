# Lista de chamadas para interagir com a aplicação:

Retorna todos as pessoas cadastradas na base:

	- http://localhost:8080/GrafoAmigos/friends/getAll
	
Retorna os amigos da pessoa solicitada (passar o id por parâmetro: 1 Arthur, 2 Mari, 3 Eduardo, 4 Gabriel):

	- http://localhost:8080/GrafoAmigos/friends/getFriends/1

# Configurando base de dados
	
Foi utilizado o PostgreSQL

Faça o download do SGBD pelo link: 

	- https://www.postgresql.org/download/

Senha configurada no projeto para acesso ao banco de dados: postgres
	
Crie o banco de dados com o nome: friends

Crie as tabelas com o script a seguir:

		create table people (
			id serial primary key not null,
			name varchar(50) not null
		);

		create table friendship (
			id serial primary key not null,
			person_node integer not null,
			friend varchar(50) not null,
			friend_id integer not null
		);

Insira os dados com o script a seguir:

		INSERT INTO people(name) VALUES (‘Arthur’);
		INSERT INTO people(name) VALUES (‘Mari’);
		INSERT INTO people(name) VALUES (‘Eduardo’);
		INSERT INTO people(name) VALUES (‘Gabriel’);

		INSERT INTO friendship(person_node, friend, friend_id) VALUES (1, Mari, 2);
		INSERT INTO friendship(person_node, friend, friend_id) VALUES (1, Eduardo, 3);
		INSERT INTO friendship(person_node, friend, friend_id) VALUES (2, Arthur, 1);
		INSERT INTO friendship(person_node, friend, friend_id) VALUES (2, Eduardo, 3);
		INSERT INTO friendship(person_node, friend, friend_id) VALUES (2, Gabriel, 4);
		INSERT INTO friendship(person_node, friend, friend_id) VALUES (3, Arthur, 1);
		INSERT INTO friendship(person_node, friend, friend_id) VALUES (3, Mari, 2);
		INSERT INTO friendship(person_node, friend, friend_id) VALUES (3, Gabriel, 4);
		INSERT INTO friendship(person_node, friend, friend_id) VALUES (4, Mari, 2);
		INSERT INTO friendship(person_node, friend, friend_id) VALUES (4, Eduardo, 3);
		
# Configurando o Apache como servidor de aplicação

Instale o Apache para subir a aplicação após fazer download do código. O link para download do Apache é:

	- https://tomcat.apache.org/download-80.cgi

Importe o projeto no eclipse, configure o apache como servidor e altere o usuário e senha da classe ConnectionFactory, se necessário, para conectar com a base de dados.